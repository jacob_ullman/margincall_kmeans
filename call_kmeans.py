# Jake Ullman - Margin Call K Means

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy import stats
from sklearn.preprocessing import LabelEncoder
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler, normalize
from sklearn.decomposition import PCA

# Loading in the data
dataset = pd.read_csv('/content/drive/MyDrive/Colab Notebooks/call_set_full.csv')
print(dataset.head())

# Preprocessing
clmns = ['currency', 'agreement_type','call_type']

def call_type_to_numeric(x):
    if x=='Variation':
        return 0
    if x=='Netted':
        return 1
    if x=='Initial':
        return 2
    

# More pre-processing (converting currency and agreement type to numbers)
dataset[clmns] = dataset[clmns].astype('category')

le = LabelEncoder()
encoded_series = dataset[dataset.columns[0:2]].apply(le.fit_transform)
dataset['currency'] = encoded_series['currency']
dataset['agreement_type'] = encoded_series['agreement_type']
dataset['call_type'] = dataset['call_type'].apply(call_type_to_numeric)


# Normalize issue_time and call_complete
min_max_scaler = preprocessing.MinMaxScaler(feature_range=(1, 10))
scaled_time_call = min_max_scaler.fit_transform(dataset[dataset.columns[3:5]])
print(scaled_time_call)
scaled_time_call = pd.DataFrame(scalled_time_call)
dataset['issue_time (decimal hours UTC)'] = scaled_time_call.iloc[:, 0]
dataset['call_complete (decimal hours UTC)'] = scaled_time_call.iloc[:, 1]

dataset = dataset.dropna() 
X = dataset
print(X.head())

# Standardize the data
scaler = StandardScaler() 
scaled_df = scaler.fit_transform(X) 
  
# Normalizing the data 
normalized_df = normalize(scaled_df) 
  
# Converting the numpy array into a pandas DataFrame 
normalized_df = pd.DataFrame(normalized_df) 
  
# Reducing the dimensions of the data 
pca = PCA(n_components = 2) 
X_principal = pca.fit_transform(normalized_df) 
X_principal = pd.DataFrame(X_principal) 
X_principal.columns = ['P1', 'P2'] 
pd.set_option('display.max_columns', None) 
print(X_principal)

# Using the elbow method to find the optimal number of clusters
from sklearn.cluster import KMeans
wcss = []
for i in range(1, 11):
    kmeans = KMeans(n_clusters = i, init = 'k-means++', random_state = 42)
    kmeans.fit(X_principal)
    wcss.append(kmeans.inertia_)
plt.plot(range(1, 11), wcss)
plt.title('The Elbow Method')
plt.xlabel('Number of clusters')
plt.ylabel('WCSS (Within-Clusters-Sum-of-Squares)')
plt.show()

# Training the K-Means model on the dataset
kmeans = KMeans(n_clusters = 3, init = 'k-means++', random_state = 42)
y_kmeans = kmeans.fit_predict(X)

dataset['cluster'] = y_kmeans
print(dataset.head())

# Visualizing the clustering 
plt.scatter(X_principal['P1'], X_principal['P2'],  
           c = KMeans(n_clusters = 3).fit_predict(X_principal), cmap =plt.cm.winter) 
plt.show()

# Writing trained data to csv
# dataset.to_csv('/content/drive/MyDrive/Colab Notebooks/call_set_trained.csv')